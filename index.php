<?php

if ($_POST["numberResponse"]) {
    $response = [];
    for ($i = 0; $i < $_POST["numberResponse"]; $i++) {

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://api.chucknorris.io/jokes/random',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
        ));

        $responsecurl = curl_exec($curl);

        curl_close($curl);
        // print_r($responsecurl);
        $response[$i] = json_decode($responsecurl) ;
    }
    echo json_encode($response) ; 
} else {
    print_r("Metodo no aceptado");
}
